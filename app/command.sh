#!/bin/bash
DIRECTORY_PROJECT="${PWD::-4}"
LOG_FILE=$DIRECTORY_PROJECT/log/command.log

TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => [ BEGIN INSTALLER ]"
echo $TEXT_LOG >> $LOG_FILE
echo ".--------------------------------------------------------------------------------------.\n"
echo "|                             CHOOSE THE PROGRAM TO DOWNLOAD                           |\n"
echo "|--------------------------------------------------------------------------------------|\n"
echo "|                         [1] =>  GERENCIADOR                                          |\n"
echo "|                         [0] =>  EXIT                                                 |\n"
echo ".--------------------------------------------------------------------------------------.\n"
read ID_PROGRAM
TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => ID PROGRAM DOWNALOAD [$ID_PROGRAM]"
echo $TEXT_LOG >> $LOG_FILE
if [ $ID_PROGRAM == 1 ];
then
	echo "GERENCIADOR"
	TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => NAME PROGRAM DOWNALOAD [GERENCIADOR]"
	echo $TEXT_LOG >> $LOG_FILE
	echo "WHAT DIRECTORY IS THE APPLICATION TO BE INSTALLED IN?"
	read DIRECTORY_PROGRAM
	TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => DIRECTORY PROGRAM [$DIRECTORY_PROGRAM]"
	echo $TEXT_LOG >> $LOG_FILE
	TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => INITIAL CLONE PROGRAM"
	echo $TEXT_LOG >> $LOG_FILE
	git clone https://gitlab.com/branksit/gerenciador.git $DIRECTORY_PROGRAM/gerenciador/
	TEXT_LOG="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => CLONE PROGRAM SUCCESS"
	echo $TEXT_LOG >> $LOG_FILE
	cd $DIRECTORY_PROGRAM/gerenciador/
	composer install
elif [ $ID_PROGRAM == 0 ];
then
	TEXT_LOG+="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => [ EXIT ]"
	echo $TEXT_LOG >> $LOG_FILE
	exit 0

fi

TEXT_LOG+="$(date +%d-%m-%Y) $(date +%H\:%M\:%s) => [ FINISH INSTALLER ]"
echo $TEXT_LOG >> $LOG_FILE